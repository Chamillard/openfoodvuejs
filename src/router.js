import Vue from 'vue';
import Router from 'vue-router';
import Accueil from './components/Accueil.vue';
import Liste from './components/Liste.vue';
import Recherche from './components/Recherche.vue';

Vue.use(Router);

export default new Router({
 routes: [
  {path:'/',name:'accueil',component:Accueil},
  {path:'/liste',name:'liste',component:Liste},
  {path:'/recherche',name:'recherche',component:Recherche}
 ]
});