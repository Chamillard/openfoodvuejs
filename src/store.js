import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    listeProduits: []
  },
  getters: {
    laListeProduits: state => {
      return state.listeProduits
    }
  },
  mutations: {
    SET_PRODUIT: (state, leProduit) => {
        state.listeProduits.push(leProduit)
    },
    DELETE_PRODUIT: (state, indexProduit) => {
      state.listeProduits.splice(state.listeProduits.indexOf(indexProduit),1)
    }
  },
  actions: {
  }
});